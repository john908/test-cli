# Changelog

Todas as mudanças notáveis neste projeto serão documentadas neste arquivo.

## [2.0.0] - 2023-03-10

### Added
- Nova funcionalidade de busca por nome de usuário.
- Opção de filtro por tipo de usuário.

### Changed
- Refatoração do componente de login.
- Alteração no layout da página inicial.

### Removed
- Remoção de código desnecessário.

## [1.1.0] - 2023-02-15

### Added
- Adicionado suporte para autenticação com Facebook.

### Changed
- Atualização da biblioteca de gráficos para melhor desempenho.
- Melhorias na experiência do usuário ao preencher formulários.

### Fixed
- Corrigido um bug que impedia que os usuários fizessem logout corretamente.

## [1.0.0] - 2023-01-01

### Added
- Lançamento inicial do projeto.
- Componentes de navegação e formulários.

### Changed
- Melhorias na estrutura do projeto.
- Refatoração do código para melhor legibilidade.

### Fixed
- Corrigido um bug que impedia a exibição correta de imagens. 

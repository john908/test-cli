## :wrench: Instalação

Após a instalação dos pré-requisitos, você está pronto para a instalação do projeto. Primeiramente você precisa configurar uma chave SSH para fazer o clone do projeto. Você pode ver como configurar uma chave SSH com este guia do gitlab

O Projeto possui alguns scripts de automação que rodam em bash, então atenha-se de conseguir rodar scripts bash (principalmente se seu OS for windows). Na raiz do projeto você pode rodar o seguinte comando para instalar os pacotes **node_modules**.

```bash
~ yarn install
```

Ao rodar `yarn install`, será disparado um script de automação que ira entrar em cada uma das pastas e irá rodar o `yarn install` dentro delas. Para isso espere o fluxo terminar completamente. Caso você encontre algum erro no meio do caminho que não permita você rodar os scripts de automação, entre individualmente em cada pasta (Drogasil, Drogaraia e Shared) e rode o mesmo comando individualmente em cada pasta.

## :rocket: Rodando o projeto

Após a instalação dos pacotes, você poderá rodar o projeto em modo de desenvolvimento, para isso você pode usar o comando `yarn dev:loja` para iniciar o servidor local:

```bash
~ yarn dev:raia

ou

~ yarn dev:sil
```

Caso tenha problemas para rodar os scripts de automação, você precisa entrar individualmente na pasta da brand que você vai trabalhar, e rodar o projeto individualmente:
```bash
~ cd web/Drogaraia
~ yarn dev

ou

~ cd web/Drogasil
~ yarn dev
```

Após executar o comando acesse o localhost que estiver em seu terminal. A partir da criação deste documento, você encontrará os servidores de drogaraia e drogasil em diferentes portas, para caso queira rodar os dois ao mesmo tempo para efeitos de comparação ou analise.

  **Drogaraia** está hospedada em `http://localhost:3001`
  **Drogasil** está hospedada em `http://localhost:3002`

# :computer: Lista de Comandos úteis
Este projeto contempla uma serie de scripts de automação e comandos úteis que podem ajudar no dia a dia do desenvolvedor. Como este projeto possuí 3 pastas principais, entrar em cada uma delas para rodar os comandos necessários pode ser irritante alguns momentos. 

Neste caso pensamos em scripts que fazem este trabalho para você, aqui você verá uma lista de comandos para rodar em seu terminal para tornar sua vida mais fácil. Lembre-se que utilizar esses comandos não é obrigatório, porém não recomendamos que delete nenhum deles, e caso encontre mais problemas no meio do caminho, sinta-se a vontade para criar mais scripts para o seu dia a dia. Segue uma lista dos comandos e o que eles fazem:

- `yarn dev`: Não irá funcionar na raiz do projeto, somente em cada loja separadamente

- `yarn dev:raia`: Irá iniciar o servidor de desenvolvimento de **Drogaraia**
- `yarn dev:sil`: Irá iniciar o servidor de desenvolvimento de **Drogasil**
- `yarn install`: Irá instalar todas as dependências de todas as pastas
- `yarn run:clean`: Irá deletar todas as *node_modules* individualmente em cada pasta
- `yarn test`: Irá rodar toda a rotina de testes de todas as pastas
- `yarn build`: Irá rodar todos os builds de todas as pastas
- `yarn start`: Não irá funcionar na raiz do projeto, somente em cada loja separadamente
- `yarn start:raia`: Irá iniciar o servidor de produção de **Drogaraia**
- `yarn start:sil`: Irá iniciar o servidor de produção de **Drogasil**

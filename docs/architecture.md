# :file_folder: A arquitetura da aplicação
Neste arquivo você encontrará uma breve explicação da arquitetura utilizada para a construção desse template e também como seguir para a construção da sua BC.

Este repositório foi projetado para se adaptar e modificar de uma maneira isolada e independente de outros módulos, assim você consegue ter uma maior velocidade de entrega e controle de qualidade do que está sendo entregue no projeto. Neste tópico vamos abordar o que é cada pasta e arquivo e o porque deles existirem.

## :thinking: O grande dilema 
Quando o desafio foi apresentado de criar um projeto que precisava ser dividido em várias partes para cada squad e que também fosse dividido em 2 sites diferentes (Drogaraia e Drogasil) foi levantado diversas maneiras de fazer isso, pensamos em juntar as lojas e rodar um unico build, mas isso teria um alto acoplamento de uma loja com a outra, ou separar cada loja em seu unico build e ela ter uma "Sub-lib" que serviria o código necessário para ambas as lojas. Foi então que definimos o núcleo da arquitetura em 3 pastas (Drogaraia, Drogasil e Shared), com essas 3 pastas elas conseguem abranger tanto coisas que existem em comum entre as 2 lojas, quanto detalhes especificos de cada uma. E se, no futuro, for realizada a compra de outra loja, ela pode ser facilmente adicionada no projeto, sem que atrapalhe o funcionamento das outras.

## :swimmer: Mergulhando mais a fundo
Vamos agora então entender melhor como o projeto está estruturado e o que significa cada uma de suas pastas:

 - **`/.husky`**: é a pasta responsável por guardar os scripts de gatilhos para validação do código no momento do commit e do push

 - **`/docs`**: é a pasta responsável por guardar toda a documentação do projeto, inclusive esta que você está lendo

- **`/scripts`**: é a pasta resposável por guardar todos os scripts de automação para o dia a dia do desenvolvedor

- **`/web`**: é onde está o core do projeto, onde reside todo o código fonte, ela será detalhada mais a frente

- **`.editorconfig`**: é um arquivo de configuração utilizado para definir e manter estilos de codificação consistentes entre diferentes editores e IDEs (Ambientes de Desenvolvimento Integrado) em um projeto.

- **`.eslintignore`**: é um arquivo para ignorar que o eslint rode em diretorios desnecessarios, como node_modules, .next e outros.

- **`.eslintsrc`**: é um arquivo de configuração utilizado pelo ESLint, uma ferramenta popular para a analise de qualidade de código. 

- **`.gitignore`**: este arquivo informa ao git quais arquivos ele não deve rastrear/não manter um histórico de versões.

- **`.prettierignore`**: é um arquivo para ignorar que o prettier rode em diretorios desnecessarios, como node_modules

- **`.prettierrc`**: é um arquivo de configuração utilizado pelo Prettier, uma ferramenta popular de formatação de código para JavaScript, TypeScript, HTML, CSS e outras linguagens. 

- **`catalog-info.yaml`**: é um arquivo que configura esta documentação dentro do DevPortal para que ele consiga catalogar o projeto.

- **`changelog.example.md`**: é um arquivo de exemplo que servirá como log do que foi entregue pela Squad.

- **`commitlint.config.js`**: é um arquivo que configura as regras de abertura de branchs e regras do padrão de commit para o projeto.

- **`jest.config.base.js`**: é um arquivo que configura os testes automatizados que devem ser rodados na aplicação, este arquivo possui a extensão `.base`  o que  significa que ele influência nos 3 projetos (Drogaraia, Drogasil e Shared).

- **`mkdocs.yaml`**: é um arquivo que configura e organiza os tópicos e plugins para a documentação no DevPortal.

- **`next-config.js`**:  É um arquivo de configuração opcional usado em projetos Next.js para personalizar e estender o comportamento padrão do framework. Ele permite que você ajuste a configuração do Next.js para atender às necessidades específicas do seu projeto.

- **`package.json`**: É um arquivo fundamental em projetos Node.js e desempenha várias funções importantes. Ele é usado para armazenar informações sobre o projeto e gerenciar dependências, scripts e configurações.
 
- **`README.md`**: É um documento de texto simples, geralmente escrito em formato Markdown, que fornece informações e instruções sobre um projeto de software. Ele é tipicamente colocado na raiz do projeto e serve como uma introdução e um guia para os desenvolvedores, colaboradores e usuários.

- **`settings.json`**: É um arquivo para configuração de contextos gerais.

- **`tsconfig.base.json`**: É um arquivo de configuração para o TypeScript, para especificar as opções e configurações do compilador TypeScript. Este arquivo possui a extensão `.base`  o que  significa que ele influência nos 3 projetos (Drogaraia, Drogasil e Shared).

- **`yarn.lock`**: É um arquivo gerado automaticamente pelo gerenciador de pacotes (yarn) ao instalar ou atualizar pacotes em um projeto Node.js. Ele serve para registrar informações detalhadas sobre as versões exatas de cada pacote e suas dependências instaladas no projeto.

## :gear: Explorando o core
Bom agora que explicamos um pouco melhor o que cada arquivo representa vamos explorar um pouco mais a pasta **`/web`** e entender o funcionamento e as responsabilidades de cada pasta dentro do core do projeto.

Dentro da pasta **`/web`** você vai encontrar 3 pastas:
 - **`/Drogaraia`**
 - **`/Drogasil`**
 - **`/Shared`**

 ![File_Structure](/docs/assets/file_structure.png)

**`/Drogaraia`** e **`/Drogasil`** possuem a mesma arquitetura, diferenciando somente uma loja da outra, já para a **`/Shared`** ela é quase identica as anteriores, mas tem configurações diferentes por ser uma "Sub-lib". Agora vamos por cada pasta e explicar suas responsabilidades:

 - **`/config`**: é a pasta responsável por guardar configurações globais que podem ser usadas em toda aplicação.

 - **`/public`**: é a pasta responsável por guardar o código e assets que precisam ficar expostos de forma publica para o site consumir.

 - **`/src`**: é a pasta responsável por guardar todo o código fonte da aplicação, dito isso vamos entrar um pouco mais a fundo dessa pasta.

    - **`/@types`**: é a pasta responsável por guardar as tipagens mais generalistas do Typescript, para que você tenha em um só lugar as tipagens mais comums do sistema.

    - **`/assets`**: é a pasta responsável por guardar todos os arquivos de imagens, videos e audios do sistema, tudo que for necessário para a construção das páginas que não seja código.
    
    - **`/components`**: é a pasta responsável por guardar todos os componentes da aplicação, é dela que vem todos os modulos necessário para construir as telas.

    - **`/containers`**: é a pasta responsável por guardar todo o conteúdo e dependências das páginas da aplicação, desta forma a responsabilidade e conteúdo de cada página fica separada e encapsulada individualmente.

    - **`/contexts`**: é a pasta responsável por guardar todos os arquivos de contextos que são utilizados para compartilhar as informações entre componentes e páginas do sistema.

    - **`/hooks`**: é a pasta responsável por guardar todos os hooks personalizados que forem criados no sistema.

    - **`/pages`**: é a pasta responsável por guardar e organizar todas as rotas da aplicação, dentro dessa pasta existem os arquivos necessários para as rotas do site. Lembrando que todo o conteúdo dessas paginas estão dentro da pasta `/containers`.

    - **`/services`**: é a pasta responsável por guardar todas as chamadas para a API, trazendo assim os dados de somente um lugar, para que facilite a manutenção do código.
    
    - **`/styles`**: é a pasta responsável por guardar todos os estilos gerais da aplicação, dentro dessa pasta existe também um reset de todas as propriedades CSS dos elementos do site.

    - **`/utils`**: é a pasta responsável por guardar todos os arquivos com funções úteis no dia a dia, como validadores, sanitizadores, verificadores e etc.
  
- **`.env.example`**: é um arquivo de exemplo que serve para armazenar e gerenciar variáveis de ambiente específicas do projeto. Você precisará do seu .env para que consiga rodar o projeto.

- **`jest.config.js`**: é um arquivo que configura os testes automatizados que devem ser rodados na aplicação

- **`/next-env.d.ts`**: É um arquivo de definição de tipos TypeScript específico para projetos Next.js. Ele serve principalmente para incluir e estender os tipos padrão do Next.js e garantir que o TypeScript reconheça e compreenda as importações de arquivos estáticos, como imagens e outros recursos.

- **`setupTests.js`**: é um arquivo que configura os testes automatizados que devem ser rodados na aplicação

- **`package.json`**: É um arquivo fundamental em projetos Node.js e desempenha várias funções importantes. Ele é usado para armazenar informações sobre o projeto e gerenciar dependências, scripts e configurações.

- **`tsconfig.jest.json`**: É um arquivo de configuração para o TypeScript, para especificar as opções e configurações do compilador TypeScript, porém com excessões para o Jest.

- **`tsconfig.json`**: É um arquivo de configuração para o TypeScript, para especificar as opções e configurações do compilador TypeScript.

- **`yarn.lock`**: É um arquivo gerado automaticamente pelo gerenciador de pacotes (yarn) ao instalar ou atualizar pacotes em um projeto Node.js. Ele serve para registrar informações detalhadas sobre as versões exatas de cada pacote e suas dependências instaladas no projeto.

Agora que repassamos por todas as pastas e arquivos que existem dentro das pastas de **`/Drogaraia`** e **`/Drogasil`** vamos falar um pouco das especificações da pasta **`/Shared`**. Como ela é uma pasta que funciona como uma "Sub-lib" dentro da aplicação, sua função é fornecer todo o código similar e necessário para as lojas consumirem. 
Sendo uma "Sub-lib" ela então não renderiza páginas, portanto as pastas `/containers` e `/pages` não são necessárias neste contexto, então elas não existem. Porém existe uma pasta a mais diferente das outras lojas, a pasta `/providers`:

- **`/providers`**: É a pasta responsável pela configuração dos contratos e comunicação com as APIs, nela você encontrará todo o código necessário para utilizar as APIs

Com tudo isso, vamos falar agora um pouco sobre as regras e boas práticas que precisam ser seguidas para que a arquitetura funcione conforme foi planejada.

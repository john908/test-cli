## :construction: Pré-requisitos

Para que você consiga instalar e rodar o projeto, é necessário ter instalado na sua máquinas algumas ferramentas:

- Node.js
- Yarn
- Terminal Bash

Primeiramente você precisa ter o **Node.js** instalado na versão `v18.15.0`, caso não tenha você pode ir até https://nodejs.org e baixar a versão ou caso já tenha outra versão do node em sua maquina você pode rodar o seguinte comando:

```bash
~ nvm install 18.15.0
```

E depois de instalado você precisa selecionar a versão baixada

```bash
~ nvm use 18.15.0
```
Após isso, você pode fazer a instalação do **Yarn**, depois da instalação do node, para que consiga fazer a instalação dos pacotes na **node_modules** e rodar o projeto

```bash
~ npm install --global yarn
```
É importante lembrar que no projeto existem alguns scripts de automação para que rodam nos ambos sites (Drogaraia e Drogasil), por isso é importante que você tenha em sua máquina algum compilador de scripts bash

> :warning: Você pode usar o próprio compilador bash do Git para rodar os scripts de automação

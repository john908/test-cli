# Como contribuir com o projeto

Para que você possa entregar suas demandas e contribuir na construção do projeto existem alguns processos e regras que precisam ser seguidas. Primeiramente temos um padrão para as branchs. Todas as branchs devem ter em seu nome o tipo de tarefa acompanhado da tag da task solicitada. Por exemplo:

```bash
feature/MICRO4X-0000 

ou

fix/MICRO4X-0000
```

Também temos um padrão para os commits. Todos os Commits devem ter em seu titulo, o prefixo do tipo de tarefa, a Tag da task juntamente com o seu número existente no Jira dentro de colchetes,seguido do texto simplificado do que foi entregue no commit. Por exemplo:

```bash
feat: [MICRO4X-0000] Adding checkout button
```

Ao adicionar a tag da task no commit, o Jira e o Gitlab farão a conexão entre elas, atrelando o commit a task que foi desenvolvida para ter um melhor tracking das entregas. Após a criação da branch e o commit e dar o push para o repositório, você deve fazer um pull request/merge request no Gitlab e solicitar um code review do código para liberar o merge no projeto.

## :wolf: Husky

Este projeto contempla a tecnologia [Husky](https://typicode.github.io/husky/) e [Commitlint](https://commitlint.js.org/#/) que são utilizadas para fazer verificações e analise do código mediante a hooks do Git. Você irá perceber que ao realizar **commits** e **pushs** para o repositório, surgirá alguns obstáculos no caminho para realizar o push final. Ao fechar um commit haverá algumas análises no código. Então vamos por partes, ao fechar um **commit** o husky irá:

1. Verificar o linting do código codado até o momento utilizando o ESLint, fazendo as devidas correções automáticas ou mostrando um erro no terminal para que você corrija.
2. Verificar a identação e estruturação do código utilizando o Prettier, para que o código esteja nos padrões corretos e estrutura correta.
3. Verificar o titulo do commmit, para que ele esteja no padrão correto e consiga ser atrelado a task do jira correspondente a demanda

Após passar por todos esses passos corretamente, o commit será fechado e estará pronto para o **push** no repositório. Ao realizar o push, o husky irá disparar a rotina de testes automatizados codados até o momento, qualquer teste que quebrar nas 3 pastas, o push será cancelado. Para que seu push seja efetivamente enviado, você precisará garantir que todos os testes estejam passando corretamente.

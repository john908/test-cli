## :raising_hand: O que é um Template?

Em termos de programação, um "Template" é uma arquitetura de código que é usado repetidamente em vários projetos, geralmente para realizar uma tarefa comum ou configurar uma estrutura básica. O Template pode ser usado como um padrão de arquitetura para ser seguida de forma com que todos os projetos tenham a mesma estrutura base, alterando somente suas especificações, assim caso um desenvolvedor precise dar manutenção em mais de uma BC ou caso ele migre de BC isso torna mais fácil a manutenção e a curva de aprendizado.

## :computer: Tecnologias

Neste projeto você encontrará diversas tecnologias que foram selecionadas para sua construção. O projeto tem como base React.js + Next.js na sua construção, por isso é importante que caso seja adicionado algum pacote ou biblioteca, que ela seja compatível com o react. 

Abaixo temos uma lista das tecnologias que você vai encontrar no projeto:

- React.js
- Next.js
- Typescript
- Yarn
- GraphQL
- Styled-Components
- Jest
- Apollo

Neste documento, você encontrará todas a informações necessárias para rodar e entender a aplicação, e a estrutura que ela foi planejada.

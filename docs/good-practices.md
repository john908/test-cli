# :thumbsup: Boas Práticas
Neste documento você verá alguns conceitos de como manter boas práticas no código e na arquitetura que foi montada, fazendo assim com que a quantidade de problemas no código diminua e facilite a manutenção.

## </> Código limpo
Código limpo é um conceito no desenvolvimento de software que enfatiza escrever código que seja fácil de entender, manter e modificar. Ele se concentra na legibilidade, simplicidade e redução da complexidade para melhorar a qualidade geral da base de código. Aqui estão alguns princípios e práticas-chave associados ao código limpo:

1. **Código legível e descritivo**: O código limpo deve ser fácil de ler e entender. Use nomes significativos e descritivos para variáveis, funções e classes. Evite abreviações e nomes criptografados ou enganosos.

2. **Simplicidade e clareza**: Mantenha o código simples e direto. Evite complexidade desnecessária, lógica complicada e aninhamento excessivo. Escreva código que expresse claramente seu propósito sem ambiguidade.

3. **Princípio da responsabilidade única (SRP)**: Cada módulo, classe ou função deve ter uma única responsabilidade. Isso ajuda a manter o código focado, coeso e mais fácil de entender e testar.

4. **Não se repita (DRY)**: Evite a duplicação de código. Em vez disso, extraia funcionalidades comuns em funções ou classes reutilizáveis. Isso reduz a redundância e torna o código mais fácil de manter.

5. **Funções e métodos pequenos**: As funções e métodos devem ser pequenos e fazer apenas uma coisa bem feita. Busque funções que se encaixem em poucas linhas e tenham um propósito claro. Isso melhora a legibilidade e torna o código mais fácil de testar e manter.

6. **Evite condicionais longos e complexos**: Declarações condicionais longas e complexas podem dificultar o acompanhamento do código. Considere o uso de técnicas como cláusulas de guarda, retornos antecipados ou polimorfismo para simplificar os condicionais e tornar o código mais legível.

7. **Comentários de código**: Use comentários com parcimônia e apenas quando necessário. Os comentários devem explicar a intenção ou a justificativa por trás do código, não apenas repetir o que o código faz. Concentre-se em escrever um código autoexplicativo, em vez de depender de comentários para explicar um código mal escrito.

8. **Testes unitários**: Escreva testes unitários para o seu código para garantir sua correção e manutenibilidade. O desenvolvimento orientado a testes (TDD) é uma prática que enfatiza a escrita de testes antes da implementação do código. Bons testes funcionam como documentação e fornecem confiança para refatorar e modificar o código.

9. **Refatoração contínua**: Refatore o código continuamente para melhorar sua estrutura e clareza. A refatoração é o processo de tornar o código mais fácil de entender e manter, sem alterar seu comportamento. Isso ajuda a eliminar dívidas técnicas e garante que a base de código permaneça limpa e gerenciável.

10. **Formatação consistente**: Aplique uma formatação e indentação consistentes em toda a base de código. Use um estilo de codificação ou guia de estilo acordado para manter uma.

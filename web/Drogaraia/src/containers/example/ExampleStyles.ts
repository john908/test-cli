import styled from 'styled-components'

export const Title = styled.h2`
  color: red;
`

export const Wrapper = styled.div`
  box-sizing: border-box;
  background-color: lightblue;
  padding: 2rem;
  width: 100%;
  height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1 {
    font-family: sans-serif;
    font-size: 2rem;
    font-weight: 600;
  }

  div {
    height: 400px;
    margin: 2rem;

    p {
      font-size: 1.2rem;
      font-family: monospace;
    }

    button {
      border: 0;
      background-color: blue;
      padding: 1rem 3rem;
      border-radius: 15px;
      color: white;
      margin-top: 1rem;
    }
  }
`

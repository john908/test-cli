import ExampleProvider from '@/contexts/example'
import ExampleContent from './ExampleContent'

export default function ExampleContainer() {
  return (
    <ExampleProvider>
      <ExampleContent />
    </ExampleProvider>
  )
}

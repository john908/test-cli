import Head from 'next/head'
import { useHookExample } from '@shared/hooks/useHookExample'
import { Title, Wrapper } from './ExampleStyles'

function ExampleContent() {
  const [value, toggleValue] = useHookExample(false)

  return (
    <>
      <Head>
        <title>Drogaraia</title>
      </Head>
      <Wrapper>
        <h1>DrogaRaia</h1>
        <div>
          <Title>Hello World!</Title>
          <p>Valor: {value.toString()}</p>
          <button type="button" onClick={toggleValue}>
            Trocar Valor
          </button>
        </div>
      </Wrapper>
    </>
  )
}

ExampleContent.propTypes = {}

export default ExampleContent

import { render } from '@testing-library/react'
import React from 'react'
import ExampleComponent from '.'

describe('Testing example: ', () => {
  test('ExampleComponent rendering correctly', () => {
    const screen = render(<ExampleComponent />)

    expect(screen.getByText('Teste')).toBeInTheDocument()
  })
})

import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import { ThemeProvider } from 'styled-components'
import tokens from '@raiadrogasil/pulso-tokens/drogasil.common'
import { client } from '../services/providers/client'
import { GlobalStyleReset } from '../styles/reset'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={tokens}>
      <GlobalStyleReset />
      <ApolloProvider client={client}>
        <Component {...pageProps} />
      </ApolloProvider>
    </ThemeProvider>
  )
}

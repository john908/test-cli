const path = require('path')

/** @type {import('next').NextConfig} */
const nextConfig = {
  eslint: {
    ignoreDuringBuilds: true,
  },
  reactStrictMode: true,
  compiler: { styledComponents: true },
  transpilePackages: ['./web/shared', './web/global'],
  webpack: config => {
    config.resolve.alias = {
      ...config.resolve.alias,
      'styled-components': path.resolve('./node_modules/styled-components/'),
    }

    return config
  },
}

module.exports = nextConfig

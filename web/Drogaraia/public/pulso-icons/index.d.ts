import { IconSet } from "./types";
export declare const icoMoonConfig: IconSet;
export * from "./constants";
export * from "./types";

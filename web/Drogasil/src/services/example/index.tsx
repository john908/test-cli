import api from '../providers/api'

interface ExampleRequestInterface {
  exampleRequest(): Promise<string>
}

async function exampleRequest(xxx: string): Promise<string> {
  return new Promise((resolve, reject) => {
    try {
      resolve(api.post(`${api}/xyz`, xxx))
    } catch (error) {
      reject(error)
    }
  })
}

export default {
  exampleRequest,
} as ExampleRequestInterface

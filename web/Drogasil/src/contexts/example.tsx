import React, { createContext, useContext, ReactNode, useMemo } from 'react'

const ExampleContext = createContext<string>('default value')

interface Props {
  children: ReactNode
}

export default function ExampleProvider({ children }: Props) {
  const example = useMemo(() => 'EXEMPLO DE CONTEXTAPI', [])

  return <ExampleContext.Provider value={example}>{children}</ExampleContext.Provider>
}

export function useExample() {
  const context = useContext(ExampleContext)
  const example = context

  return {
    example,
  }
}

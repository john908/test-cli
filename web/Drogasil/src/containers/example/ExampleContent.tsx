import Head from 'next/head'
import TestComponent from '@shared/components/ExampleComponent'
import { useHookExample } from '@/hooks/useHookExample'
import { Title, Wrapper } from './ExampleStyles'

function ExampleContent() {
  const [value, toggleValue] = useHookExample(false)

  return (
    <>
      <Head>
        <title>DrogaSil</title>
      </Head>
      <Wrapper>
        <h1>DrogaSil</h1>
        <div>
          <Title>Hello World!</Title>
          <p>Valor: {value.toString()}</p>
          <TestComponent />
          <button type="button" onClick={toggleValue}>
            Trocar Valor
          </button>
        </div>
      </Wrapper>
    </>
  )
}

ExampleContent.propTypes = {}

export default ExampleContent

import { ApolloProvider } from '@apollo/client'
import { client } from '@shared/providers/client'
import ExampleContent from './ExampleContent'
import ExampleProvider from '@/contexts/example'

export default function ExampleContainer() {
  return (
    <ExampleProvider>
      <ApolloProvider client={client}>
        <ExampleContent />
      </ApolloProvider>
    </ExampleProvider>
  )
}

export declare type IconSetItem = {
    properties: {
        name: string;
    };
    icon: {
        paths: string[];
        attrs: Record<string, unknown>[];
        width?: number | string;
    };
};
export declare type IconSet = {
    icons: IconSetItem[];
};

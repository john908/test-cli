# Pulso Icons

Aqui você encontra a biblioteca de ícones, do Pulso, o Design System da RD.

Caso você não conheça o Pulso, pode acessar seu site em https://pulso.rd.com.br.

Informações sobre a arquietetura, como instalar e utilizar essa biblioteca podem ser encontradas em https://pulso-storybook.rd.com.br.

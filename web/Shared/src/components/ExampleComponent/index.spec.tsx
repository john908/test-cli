import { render } from '@testing-library/react'
import TestComponent from '.'

describe('Testing example: ', () => {
  test('ExampleComponent rendering correctly', () => {
    const screen = render(<TestComponent />)

    expect(screen.getByText('Teste Shared')).toBeInTheDocument()
  })
})

import { ApolloClient, InMemoryCache } from '@apollo/client'

import config from '../../config/environment'

export const client = new ApolloClient({
  uri: config.apis.graphql,
  cache: new InMemoryCache(),
})

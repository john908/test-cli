
interface ConfigInterface {
  apis: any
}

export default {
  apis: {
    host: process.env.EXAMPLE_API,
    graphql: process.env.NEXT_PUBLIC_GRAPHQL_API_URL,
  },
} as ConfigInterface

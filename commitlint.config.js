module.exports = {
  extends: ['@commitlint/config-conventional'],
  plugins: ['commitlint-plugin-function-rules'],
  rules: {
    'header-max-length': [0], // level: disabled
    'function-rules/header-max-length': [
      2, // level: error
      'always',
      parsed => {
        if (!parsed.type) {
          return [
            false,
            'Type must be build, chore, ci, docs, feat, fix, perf, refactor, revert, style or test',
          ]
        }

        const formatCommit = new RegExp(/\[[A-Z0-9]+-\d+\] .+/)
        if (parsed.subject.match(formatCommit)) {
          return [true]
        }
        return [
          false,
          'The message pattern must follow the following format: feat: [project name - task number] + task title. Ex: [MICRO4X-0001] add a new feature.',
        ]
      },
    ],
  },
}

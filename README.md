<div align="center">
  <img width="350" src="/docs/assets/programa4x.svg">
</div>

# :building_construction: Core-site Template

Este projeto serve como base para criação de novos microfrontends, nele explicamos tudo o que você, desenvolvedor, precisa para que entenda o projeto, suas regras e tecnologias. Neste guia você encontrará um passo a passo desde a instalação até como contribuir com o projeto.

## :raising_hand: O que é um Template?

Em termos de programação, um "Template" é um conjunto de código e estrutura que é usado repetidamente em vários projetos, geralmente para realizar uma tarefa comum ou configurar uma estrutura básica. O termo é derivado da indústria de impressão, onde "boilerplate" referia-se a placas de metal gravadas com texto padrão que poderiam ser usadas repetidamente em diferentes documentos.
## :computer: Tecnologias

Dentro deste projeto, você encontrará as seguintes tecnologias que foram definidas para que o sistema funcione de forma performática e alcance os requisitos do Programa 4X.

<p align="center">
  <img alt="Tech" src="https://img.shields.io/badge/Nodejs-000000?style=for-the-badge&logo=node.js&logoColor=339933">
  <img alt="Tech" src="https://img.shields.io/badge/React-000000?style=for-the-badge&logo=react">
  <img alt="Tech" src="https://img.shields.io/badge/Next.js-000000?style=for-the-badge&logo=next.js">
  <img alt="Tech" src="https://img.shields.io/badge/TypeScript-000000?style=for-the-badge&logo=typescript">
  <img alt="Tech" src="https://img.shields.io/badge/GraphQl-000000?style=for-the-badge&logo=graphql&logoColor=E10098">
  <img alt="Tech" src="https://img.shields.io/badge/Jest-000000?style=for-the-badge&logo=jest&logoColor=C21325">
  <img alt="Tech" src="https://img.shields.io/badge/Styled components-000000?style=for-the-badge&logo=styled-components">
  <img alt="Tech" src="https://img.shields.io/badge/Apollo-000000?style=for-the-badge&logo=apollo-graphql&logoColor=311C87">
  <img alt="Tech" src="https://img.shields.io/badge/yarn-000000?style=for-the-badge&logo=yarn">
</p>

## :construction: Pré-requisitos

Para que você consiga instalar e rodar o projeto, é necessário ter instalado na sua máquinas algumas ferramentas:

1. Node.js
2. Yarn
3. Terminal Bash

Primeiramente você precisa ter o **Node.js** instalado na versão `v18.15.0`, caso não tenha você pode ir até https://nodejs.org e baixar a versão ou caso já tenha outra versão do node em sua maquina você pode rodar o seguinte comando:

```bash
~ nvm install 18.15.0
```

E depois de instalado você precisa selecionar a versão baixada

```bash
~ nvm use 18.15.0
```
Após isso, você pode fazer a instalação do **Yarn**, depois da instalação do node, para que consiga fazer a instalação dos pacotes na **node_modules** e rodar o projeto

```bash
~ npm install --global yarn
```
É importante lembrar que no projeto existem alguns scripts de automação para que rodam nos ambos sites (Drogaraia e Drogasil), por isso é importante que você tenha em sua máquina algum compilador de scripts bash

> :warning: Você pode usar o próprio compilador bash do Git para rodar os scripts de automação


## :wrench: Instalação

Após a instalação dos pré-requisitos, você está pronto para a instalação do projeto. Primeiramente você precisa configurar uma chave SSH para fazer o clone do projeto. Você pode ver como configurar uma chave SSH com este guia do gitlab

O Projeto possui alguns scripts de automação que rodam em bash, então atenha-se de conseguir rodar scripts bash (principalmente se seu OS for windows). Na raiz do projeto você pode rodar o seguinte comando para instalar os pacotes **node_modules**.

```bash
~ yarn install
```

Após a instalação dos pacotes, você precisa entrar individualmente na pasta da brand que você vai trabalhar, e rodar o projeto individualmente

```bash
~ cd web/Drogaraia
~ yarn dev

ou

~ cd web/Drogasil
~ yarn dev
```

Após executar o comando acesse o localhost que estiver em seu terminal.
## :rotating_light: Regras de contribuição

Para que você possa entregar suas demandas existem alguns processos e regras que precisam ser seguidas. Primeiramente temos um padrão para as branchs. Todas as branchs devem ter em seu nome o tipo de tarefa acompanhado da tag da task solicitada. Por exemplo:

```bash
feature/MICRO4X-0000 

ou

fix/MICRO4X-0000
```

Também temos um padrão para os commits. Todos os Commits devem ter em seu titulo, a Tag da task juntamente com o seu número existente no Jira dentro de colchetes seguido do texto simplificado do que foi entregue no commit. Por exemplo:

```bash
feat: [MICRO4X-0000] Adding checkout button
```

Ao adicionar a tag da task no commit, o Jira e o Gitlab farão a conexão entre elas, atrelando o commit a task que foi desenvolvida para ter um melhor tracking das entregas. Após a criação da branch e o commit e dar o push para o repositório, você deve fazer um pull request/merge request no Gitlab e solicitar um code review do código para liberar o merge no projeto.


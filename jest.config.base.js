const MINIMUM_GLOBAL_COVERAGE_ACCEPTED = {
  statements: 80,
}

module.exports = {
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '/.next/'],
  setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': [
      'ts-jest',
      {
        tsconfig: 'tsconfig.jest.json',
      },
    ],
  },
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '@ui/(.*)': '<rootDir>../UI/src/components/$1',
    '@shared/(.*)': '<rootDir>../Shared/src/$1',
  },
  // collectCoverage: true,
  // coverageReporters: ['text'],
  coverageThreshold: {
    global: MINIMUM_GLOBAL_COVERAGE_ACCEPTED,
  },
  // collectCoverageFrom: [
  //   'src/**/*.tsx',
  //   '!src/@types/**/*',
  //   '!src/assets/**/*',
  //   '!src/config/**/*',
  //   '!src/styles/**/*',
  //   '!src/**/*.styles.ts',
  // ],
}

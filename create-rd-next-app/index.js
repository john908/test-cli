#!/usr/bin/env node

const fs = require('fs')
const path = require('path')

// Função para copiar e colar arquivos, ignorando subdiretórios específicos
function copyFilesWithIgnore(sourceDir, destDir, ignoreDirs) {
  // Obtém a lista de arquivos e subdiretórios no diretório de origem
  const files = fs.readdirSync(sourceDir)

  // Itera sobre cada arquivo/diretório encontrado
  files.forEach(file => {
    // Verifica se o arquivo atual é um diretório
    const filePath = path.join(sourceDir, file)
    if (fs.statSync(filePath).isDirectory()) {
      // Verifica se o diretório atual deve ser ignorado
      if (ignoreDirs.includes(file)) {
        // Ignora o diretório
        return
      } else {
        // Copia e cola o diretório, chamando a função de forma recursiva
        const newSourceDir = path.join(sourceDir, file)
        const newDestDir = path.join(destDir, file)
        copyFilesWithIgnore(newSourceDir, newDestDir, ignoreDirs)
      }
    } else {
      // Copia e cola o arquivo para o diretório de destino
      const destFilePath = path.join(destDir, file)
      fs.cpSync(filePath, destFilePath)
    }
  })
}

const run = () => {
  console.log('create project', process.cwd())
  console.log('dir')

  const sourceDir = `${__dirname}/../`
  const destDir = `${process.cwd()}/core-site/`
  const ignoreDirs = ['node_modules', '.next', 'create-rd-next-app']

  copyFilesWithIgnore(sourceDir, destDir, ignoreDirs)
}

run()

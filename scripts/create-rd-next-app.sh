#!/usr/bin/env bash

CYAN='\033[0;46m'
NEUTRAL='\033[0;0m'

echo "basename: [$(basename "$0")]"
echo "dirname : [$(dirname "$0")]"
echo "pwd     : [$(pwd)]"

cd create-rd-next-app
echo " "
echo " "
echo " "
echo -e "${GREEN}Executando instalação dos pacotes em Drogaraia...${NEUTRAL}"
yarn install

yarn run app:create

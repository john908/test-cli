#!/usr/bin/env bash

CYAN='\033[0;46m'
NEUTRAL='\033[0;0m'

cd web/Drogaraia
echo " "
echo " "
echo " "
echo "${CYAN}Executando testes em Drogaraia...${NEUTRAL}"
yarn test

cd ..
cd Drogasil
echo " "
echo " "
echo " "
echo "${CYAN}Executando testes em Drogasil...${NEUTRAL}"
yarn test

cd ..
cd Shared
echo " "
echo " "
echo " "
echo "${CYAN}Executando testes em Shared...${NEUTRAL}"
yarn test

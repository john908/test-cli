#!/usr/bin/env bash

RED='\033[0;41m'
NEUTRAL='\033[0;0m'

cd web/Drogaraia
echo " "
echo " "
echo " "
echo -e "${RED}Excluindo node_modules em Drogaraia...${NEUTRAL}"
rm -rf node_modules

cd ..
cd Drogasil
echo " "
echo " "
echo " "
echo -e "${RED}Excluindo node_modules em Drogasil...${NEUTRAL}"
rm -rf node_modules

cd ..
cd Shared
echo " "
echo " "
echo " "
echo -e "${RED}Excluindo node_modules em Shared...${NEUTRAL}"
rm -rf node_modules

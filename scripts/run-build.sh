#!/usr/bin/env bash

BLUE='\033[0;44m'
NEUTRAL='\033[0;0m'

cd web/Drogaraia
echo " "
echo " "
echo " "
echo -e "${BLUE}Executando build em Drogaraia...${NEUTRAL}"
yarn build

cd ..
cd Drogasil
echo " "
echo " "
echo " "
echo -e "${BLUE}Executando build em Drogasil...${NEUTRAL}"
yarn build

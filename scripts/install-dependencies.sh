#!/usr/bin/env bash

GREEN='\033[0;42m'
NEUTRAL='\033[0;0m'

cd web/Drogaraia
echo " "
echo " "
echo " "
echo -e "${GREEN}Executando instalação dos pacotes em Drogaraia...${NEUTRAL}"
yarn install

cd ..
cd Drogasil
echo " "
echo " "
echo " "
echo -e "${GREEN}Executando instalação dos pacotes em Drogasil...${NEUTRAL}"
yarn install

cd ..
cd Shared
echo " "
echo " "
echo " "
echo -e "${GREEN}Executando instalação dos pacotes em Shared...${NEUTRAL}"
yarn install

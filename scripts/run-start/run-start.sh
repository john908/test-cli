#!/usr/bin/env bash

RED='\033[0;41m'
NEUTRAL='\033[0;0m'

echo " "
echo -e "${RED}O Comando Start funciona somente se executado em cada loja separadamente, tente usar start:loja no terminal para selecionar a opção que deseja ${NEUTRAL}"
exit


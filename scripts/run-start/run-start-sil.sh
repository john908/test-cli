#!/usr/bin/env bash

RED='\033[0;41m'
NEUTRAL='\033[0;0m'

cd web/Drogasil
echo " "
echo -e "Executando servidor em ${RED} DROGASIL ${NEUTRAL}"
yarn start

#!/usr/bin/env bash

CYAN='\033[0;46m'
NEUTRAL='\033[0;0m'

cd web/Drogaraia
echo " "
echo -e "Executando servidor em ${CYAN} DROGARAIA ${NEUTRAL}"
yarn start

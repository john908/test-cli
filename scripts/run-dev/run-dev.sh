#!/usr/bin/env bash

RED='\033[0;41m'
NEUTRAL='\033[0;0m'

echo " "
echo -e "${RED}O Comando dev não deve ser executado na raiz do projeto. Funciona somente se executado em cada loja separadamente, tente usar dev:loja no terminal para rodar a opção que deseja ou entre em cada pasta individualmente ${NEUTRAL}"
exit
